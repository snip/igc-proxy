<?php
function isIgcFile($igc_file) {
    // Source : http://vali.fai-civl.org/documents/IGC-Spec_v1.00.pdf

    // Condition : comencer par un A
    if (substr($igc_file, 0, 1) !== 'A')
        return false;

    $lines = explode("\n", $igc_file);

    // Condition : au moins 9 lignes
    if (count($lines) < 9)
        return false;

    foreach ($lines as $line) {
        if ($line === "")
	    continue;

	// Condition : chaque ligne ne doit pas dépasser 176 caractères (normalement 76)
        if (strlen($line) > 176 && !in_array(substr($line, 0, 1), array('H', 'L')))
	    return false;

        // Condition : chaque ligne doit commencer par une lettre de A à M
	if (!in_array(substr($line, 0, 1), array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M')))
	    return false;
    }
    return true;
}

$url = $_GET['url'];
$url_infos = parse_url($url);

// Contrôler le nom de domaine
if (  !preg_match('`\.netcoupe\.net$`', $url_infos['host'])
   && !preg_match('`\.soaringspot\.com$`', $url_infos['host'])
   && !preg_match('`picman\.cvvm\.fr$`', $url_infos['host'])
   && !preg_match('`planeur-aacm\.net$`', $url_infos['host'])
   ) {
    file_put_contents('/var/log/igc_proxy.err', "Error domain $url\n", FILE_APPEND);
    http_response_code(400);
    print('Not allowed.');
    exit;
}

// Récupérer le fichier
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
// Spécifier l'adresse IP à l'origine de la requête
$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
curl_setopt($ch, CURLOPT_HTTPHEADER, array("REMOTE_ADDR: $ip", "X_FORWARDED_FOR: $ip"));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_USERAGENT, "igc-proxy");
$igc_file = curl_exec($ch);
curl_close($ch);

// Contrôler le fichier
if (!isIgcFile($igc_file)) {
    file_put_contents('/var/log/igc_proxy.err', "Error format $url\n", FILE_APPEND);
    http_response_code(500);
    print('Error.');
    exit;
}

// Fournir le fichier
header('Content-Type: text/plain');
print($igc_file);
