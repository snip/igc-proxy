# igc-proxy

Proxy pour des fichiers IGC avec vérification du domaine et du format de fichier.


## Log des erreurs
L'action suivante est à réaliser pour permettre l'enregistrement des erreurs.

```
touch /var/log/igc_proxy.err
chown www-data:www-data /var/log/igc_proxy.err
```
